const http = require("http");
const express = require('express');
const bodyParser = require('body-parser');
//const app = express();
const multer = require("multer");
const path = require("path");
const mime = require('mime');
const fs = require('fs');
const app = require('express')();


//const dotenv = require('dotenv's


app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.json())
const storage = multer.diskStorage({
    destination: './upload/images',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`)
    }
})
const filefilter = (req, file, cb) => {
    //reject a file
    // if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
    cb(null, true);
    // }
    // else {
    //     cb(null, false);
    // }
}
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 10000000000
    },
    fileFilter: filefilter,
})
//Inserting the list of all product details to database.

app.post("/upload", upload.single('photo'), (req, res) => {
    console.log(req.body
    )
    var matches = req.body.photo.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    let decodedImg = response;
    let imageBuffer = decodedImg.data;
    let type = decodedImg.type;
    // let extension = mime.extension(type);
    let file = "image." + "jpeg";
    let fileName1 = "image" + Date.now() + "." + "jpeg"


    try {
        console.log("oooo")
        fs.writeFileSync("./upload/images/" + fileName1, imageBuffer, 'utf8');
        // return res.send({ "status": "success" });
    } catch (e) {
        console.log(e)
        next(e);
    }





})

app.use((req, res, next) => {
    res.status(400).send('<h1>page not found</h1>');
});
app.post('/k', (req, res, next) => {
    console.log("kk")
    // res.status(400).send('<h1>page not found</h1>');
});
app.listen(4000, () => {
    console.log("server up and running");
});
module.exports = app;
